def searchInTab(array, elementToFlag):
    """Function return the postion of element or False in tab non sorted

    Args:
        array (list): non sorted array
        elementToFlag (int): element who want the position

    Returns:
        int or bool: int if position has been found, false if hasn't found
    """
    lenTab = len(array)
    index = 0
    while index < lenTab:
        if array[index] == elementToFlag:
            return index
        index += 1
    return False


def searchInSortedTab(array, elementToFlag):
    """Function to return the position of element in a sorted tab

    Args:
        array (list): sorted array
        elementToFlag (int): element who want the position

    Returns:
        int or bool: int if position has been found, false if hasn't found
    """
    lenTab = len(array)
    index = 0
    while index < lenTab and array[index] <= elementToFlag:
        if array[index] == elementToFlag:
            return index
        index += 1
    return False


def termeDeLaSuite(firstTerm, r, n):
    """Function to have the value of Un term in serie

    Args:
        firstTerm (int): first term of serie
        r (int): reason of serie
        n (int): n

    Returns:
        int: U(n)
    """
    index = 0
    while index < n:
        if index == 0:
            U = firstTerm
        else:
            U = U + r
        index += 1
    return U


def termeDeLaSuiteRecursif(firstTerme, raison, n):
    """Function to have the value of Un term in serie

    Args:
        firstTerm (int): first term of serie
        r (int): reason of serie
        n (int): n

    Returns:
        int: U(n)
    """
    return firstTerme if n <= 1 else termeDeLaSuiteRecursif(firstTerme, raison, n-1) + raison


def dicotomique(sortedTab, elementToFlag):
    lenTab = len(sortedTab)
    if lenTab == 1:
        return 0
    middle = lenTab//2
    if sortedTab[middle] == elementToFlag:
        return middle
    elif sortedTab[middle] > elementToFlag:
        return dicotomique(sortedTab[:middle], elementToFlag)
    else:
        return middle + dicotomique(sortedTab[middle:], elementToFlag)


if __name__ == "__main__":
    array = [1, 2, 3, 4, 5, 6]
    array = [1, 2, 3, 4, 5, 6]

    print("position :", searchInTab(array, 3))
    print("position :", searchInTab(array, 7))
    print("position :", searchInSortedTab(array, 4))
    print("position :", searchInSortedTab(array, 7))
    print(termeDeLaSuite(1, 2, 5))
    print(termeDeLaSuite(1, 2, 2))
    print(termeDeLaSuiteRecursif(1, 2, 5))
    print(termeDeLaSuiteRecursif(1, 2, 2))

    print("position :", dicotomique(array, 4))
    print("position :", dicotomique(array, 7))
