def permutationQuadratique(tableau):
    index1 = 0
    isPermutation = True
    while index1 < len(tableau) and isPermutation == True:
        index2 = 0
        while index2 < len(tableau) and isPermutation == True:
            if index1 != index2 and tableau[index1] == tableau[index2] and tableau[index1] > 0 and tableau[index1] <= len(tableau):
                isPermutation = False
            index2 += 1
        index1 += 1
    return isPermutation


def permutationLineaire(tableau):
    TabOccurence = []
    for i in range(len(tableau) + 1):
        TabOccurence.append(0)
    for i in tableau:
        if i > 0 or i <= len(tableau):
            TabOccurence[i] += 1
            if TabOccurence[i] > 1:
                return False
        else:
            return False
    return True


if __name__ == "__main__":
    tableau = [1, 2, 3, 4, 5, 6]
    tableau2 = [1, 3, 4, 5, 5]
    tableau3 = [-1, 3, 4, 5, 5]

    print(permutationQuadratique(tableau))  # True
    print(permutationQuadratique(tableau2))  # False
    print(permutationQuadratique(tableau3))  # False
    print(permutationLineaire(tableau))  # True
    print(permutationLineaire(tableau2))  # False
    print(permutationLineaire(tableau3))  # False
