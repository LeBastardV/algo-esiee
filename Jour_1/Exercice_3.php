<?php
function permutationQuadratique($tableau)
{
    $index1 = 0;
    $isPermutation = true;
    $arrayLenght = sizeof($tableau);
    while ($index1 < $arrayLenght && $isPermutation) {
        $index2 = 0;
        while ($index2 < $arrayLenght && $isPermutation) {
            if ($index1 != $index2 && $tableau[$index1] == $tableau[$index2] && $tableau[$index1] > 0 && $tableau[$index1] <= $arrayLenght) {
                return false;
            }
            $index2 += 1;
        }
        $index1 += 1;
    }
    return $isPermutation;
}

function permutationLineaire($tableau)
{
    $tabOccurence = array();
    $arrayLenght = sizeof($tableau);
    for ($index = 0; $index < $arrayLenght; $index++) {
        $tabOccurence[$index] = 0;
    }

    foreach ($tableau as $value) {
        if ($value > 0 && $value <= $arrayLenght) {
            $tabOccurence[$value] += 1;
            if ($tabOccurence[$value] > 1) {
                return false;
            }
        } else {
            return false;
        }
    }
    return true;
}

$array1 = array(1, 2, 3, 4, 5, 6);
$array2 = array(1, 2, 3, 4, 5, 5);
$array3 = array(-1, 3, 4, 5, 5);

print_r($array1);
echo permutationQuadratique($array1) ? 'true <br>' : 'false <br>';
print_r($array2);
echo permutationQuadratique($array2) ? 'true <br>' : 'false <br>';
print_r($array3);
echo permutationQuadratique($array3) ? 'true <br>' : 'false <br>';

print_r($array1);
echo permutationLineaire($array1) ? 'true <br>' : 'false <br>';
print_r($array1);
echo permutationLineaire($array2) ? 'true <br>' : 'false <br>';
print_r($array1);
echo permutationLineaire($array3) ? 'true <br>' : 'false <br>';
