import numpy as np
import matplotlib.pyplot as plt

n = range(1, 100)
Ya = np.log(n)
Yb = n
Yc = n * np.log10(n)
Yd = [2 ** val for val in n]
Ye = [val ** 2 for val in n]


plt.plot(n, Ya, label="f(n)=log(n)")
plt.plot(n, Yb, label="f(n)=n")
plt.plot(n, Yc, label="f(n)=n log(n)")
plt.plot(n, Yd, label="f(n)=2^n")
plt.plot(n, Ye, label="f(n)=n^2")
plt.ylim(0, 1000)
plt.legend()
plt.show()
